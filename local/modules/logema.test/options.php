<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}

if (!$USER->IsAdmin()) {
	return;
}

CAdminMessage::showMessage(array(
	"MESSAGE" => "Нет настроек",
	"TYPE" => "OK",
));
