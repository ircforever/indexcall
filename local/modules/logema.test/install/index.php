<?php

use Bitrix\Main\ModuleManager;

class logema_test extends CModule
{
	public $MODULE_ID = "logema.test";
	public $MODULE_VERSION;
	public $MODULE_VERSION_DATE;
	public $MODULE_NAME;
	public $MODULE_DESCRIPTION;

	public function __construct()
	{
		$arModuleVersion = array();

		include(__DIR__ ."/version.php");

		$this->MODULE_VERSION = $arModuleVersion["VERSION"];
		$this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
		$this->PARTNER_NAME = "Logema";
		$this->PARTNER_URI = "http://logema.org/";

		$this->MODULE_NAME = "Logema.Test";
		$this->MODULE_DESCRIPTION = "Logema.Test";
	}

	public function DoInstall()
	{
		ModuleManager::registerModule($this->MODULE_ID);
	}

	public function DoUninstall()
	{
		ModuleManager::unRegisterModule($this->MODULE_ID);
	}
}
