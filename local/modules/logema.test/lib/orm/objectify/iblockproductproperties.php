<?php

namespace Logema\Test\Orm\Objectify;

use Bitrix\Iblock\PropertyEnumerationTable;
use Bitrix\Main\SystemException;
use Logema\Test\Config;
use Logema\Test\Orm\Entity\EO_IblockProductProperties;

class IblockProductProperties extends EO_IblockProductProperties
{
	/**
	 * @param $value
	 * @return int
	 * @throws SystemException
	 * @throws \Exception
	 */
	public static function getSizeEnumId($value = null)
	{
		$enum = PropertyEnumerationTable::query()
			->setSelect(["*"])
			->where("PROPERTY_ID", Config::PRODUCT_IBLOCK_SIZE_PROP_ID)
			->where("VALUE", $value)
			->exec()
			->fetch();

		if (!$enum && !$value) {
			$enum = PropertyEnumerationTable::query()
				->setSelect(["*"])
				->where("PROPERTY_ID", Config::PRODUCT_IBLOCK_SIZE_PROP_ID)
				->where("DEF", "Y")
				->exec()
				->fetch();
		}

		if ($enum) {
			return $enum["ID"];
		} else {

			$result = PropertyEnumerationTable::add([
				"PROPERTY_ID" => Config::PRODUCT_IBLOCK_SIZE_PROP_ID,
				"VALUE" => $value,
				"DEF" => "N",
				"XML_ID" => md5(uniqid("", true)),
			]);

			if(CACHED_b_iblock_property_enum !== false)
				$GLOBALS["CACHE_MANAGER"]->CleanDir("b_iblock_property_enum");

			if (defined("BX_COMP_MANAGED_CACHE"))
				$GLOBALS["CACHE_MANAGER"]->ClearByTag("iblock_property_enum_".Config::PRODUCT_IBLOCK_SIZE_PROP_ID);

			if ($result->isSuccess()) {
				return $result->getId();
			}
		}

		return null;
	}

	/**
	 * @param mixed|null $enumId
	 * @return mixed|null
	 * @throws SystemException
	 */
	public static function getSizeEnumValue($enumId = null)
	{
		$enum = PropertyEnumerationTable::query()
			->setSelect(["*"])
			->where("ID", $enumId)
			->where("PROPERTY_ID", Config::PRODUCT_IBLOCK_SIZE_PROP_ID)
			->exec()
			->fetch();

		if ($enum) {
			return $enum["VALUE"];
		}

		return null;
	}
}
