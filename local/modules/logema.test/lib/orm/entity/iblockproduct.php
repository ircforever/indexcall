<?php
namespace Logema\Test\Orm\Entity;

use Bitrix\Iblock\ElementTable;
use Bitrix\Main\ORM;
use Bitrix\Main\ORM\Event;
use Bitrix\Main\ORM\EventResult;
use Bitrix\Main\ORM\Fields\Relations\Reference;
use Bitrix\Main\SystemException;
use Logema\Test\Config;
use Logema\Test\Orm\Objectify;

/**
 * Class IblockProductTable
 *
 * Свойства в отдельной таблице
 *
 * @package Logema\Test\Orm\Entity
 */
class IblockProductTable extends ORM\Data\DataManager
{
	public static function getTableName()
	{
		return ElementTable::getTableName();
	}

	public static function getObjectClass()
	{
		return Objectify\IblockProduct::class;
	}

	public static function getCollectionClass()
	{
		return Objectify\IblockProductCollection::class;
	}

	/**
	 * @return array
	 * @throws SystemException
	 */
	public static function getMap()
	{
		return array_merge(ElementTable::getMap(), [
			(new Reference('PROPERTIES', IblockProductPropertiesTable::class, ['=this.ID' => 'ref.ID']))
				->configureJoinType(ORM\Query\Join::TYPE_LEFT)
		]);
	}

	public static function onBeforeAdd(Event $event)
	{
		$result = new EventResult();
		$result->modifyFields(['IBLOCK_ID' => Config::PRODUCT_IBLOCK_ID]);

		return $result;
	}

	public static function onAfterAdd(Event $event)
	{
		/** @var Objectify\IblockProduct $object */
		$object = $event->getParameter('object');

		if ($props = $object->getProperties()) {
			$props->setId($object->getId())->save();
		}

		return new EventResult();
	}

	public static function onAfterUpdate(Event $event)
	{
		/** @var Objectify\IblockProduct $object */
		$object = $event->getParameter('object');

		if ($props = $object->getProperties()) {
			if ($props->state == ORM\Objectify\State::RAW) {
				$props->setId($object->getId())->save();
			}
		}

		return new EventResult();
	}

	/**
	 * @param Event $event
	 * @return EventResult|void
	 * @throws \Exception
	 */
	public static function onAfterDelete(Event $event)
	{
		$primary = $event->getParameter('primary');

		if ($primary) {
			IblockProductPropertiesTable::delete($primary['ID']);
		}

		return new EventResult();
	}
}