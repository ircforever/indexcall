<?php
namespace Logema\Test\Orm\Entity;

use Bitrix\Iblock\PropertyEnumerationTable;
use Bitrix\Main\ORM\Data\DataManager;
use Bitrix\Main\ORM;
use Bitrix\Main\ORM\Fields;
use Bitrix\Main\ORM\Fields\Relations\Reference;
use Bitrix\Main\ORM\Query\Filter\ConditionTree;
use Bitrix\Main\SystemException;
use Logema\Test\Config;
use Logema\Test\Orm\Objectify;

/**
 * Class IblockProductPropertiesTable
 *
 * Для двух конкретных НЕмножественных свойств
 *
 * @package Logema\Test\Orm\Entity
 */
class IblockProductPropertiesTable extends DataManager
{
	public static function getTableName()
	{
		return 'b_iblock_element_prop_s' . Config::PRODUCT_IBLOCK_ID;
	}

	public static function getObjectClass()
	{
		return Objectify\IblockProductProperties::class;
	}

	/**
	 * @return array
	 * @throws SystemException
	 */
	public static function getMap()
	{
		return [
			(new Fields\IntegerField('ID'))
				->configurePrimary(true)
				->configureColumnName('IBLOCK_ELEMENT_ID'),

			(new Fields\StringField('COLOR'))
				->configureColumnName('PROPERTY_' . Config::PRODUCT_IBLOCK_COLOR_PROP_ID),

			(new Fields\IntegerField('SIZE'))
				->configureColumnName('PROPERTY_' . Config::PRODUCT_IBLOCK_SIZE_PROP_ID),

			(new Reference('SIZE_ENUM', PropertyEnumerationTable::class,
				(new ConditionTree())
					->whereColumn('this.SIZE', 'ref.ID')
					->where('ref.PROPERTY_ID', Config::PRODUCT_IBLOCK_SIZE_PROP_ID)))
				->configureJoinType(ORM\Query\Join::TYPE_LEFT),

			(new Reference('ELEMENT', IblockProductTable::class, ['=this.ID' => 'ref.ID']))
				->configureJoinType(ORM\Query\Join::TYPE_LEFT)
		];
	}
}