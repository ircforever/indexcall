<?php
namespace Logema\Test;


class Config
{
	public const PRODUCT_IBLOCK_ID = 1;
	public const PRODUCT_IBLOCK_CODE = 'product';
	public const PRODUCT_IBLOCK_COLOR_PROP_ID = 1;
	public const PRODUCT_IBLOCK_COLOR_PROP_CODE = 'COLOR';
	public const PRODUCT_IBLOCK_SIZE_PROP_ID = 2;
	public const PRODUCT_IBLOCK_SIZE_PROP_CODE = 'SIZE';
}