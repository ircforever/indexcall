<?php
namespace Logema\Test;


use Bitrix\Main\ORM\Data\AddResult;
use Bitrix\Main\ORM\Data\DeleteResult;
use Bitrix\Main\ORM\Data\UpdateResult;
use Bitrix\Main\SystemException as BitrixException;
use Logema\Test\Orm\Entity\IblockProductTable as ProductTable;
use Logema\Test\Orm\Objectify\IblockProduct as ProductObject;
use Logema\Test\Orm\Objectify\IblockProductProperties as ProductPropertiesObject;


/**
 * Class ProductManager
 *
 * Для двух конкретных свойств при условии хранения свойств в отдельной таблице
 *
 * Через ORM для инфоблоков:
 * \Bitrix\Iblock\Elements\ElementProductTable, \Bitrix\Iblock\Elements\EO_ElementProduct и т.д.
 * не работаем из-за багов. (В основном связаны с неправильным сохранением свойств)
 *
 * @package Logema\Test
 */
class ProductManager
{
	/**
	 * @param $id
	 * @return array|null
	 * @throws BitrixException
	 */
	public static function GetInfo($id)
	{
		/** @var ProductObject $element */
		$element = ProductTable::query()
			->setSelect(['*', 'PROPERTIES', 'PROPERTIES.SIZE_ENUM'])
			->where('ID', $id)
			->exec()
			->fetchObject();

		if ($element) {
			$props = $element->getProperties();
			$sizeEnum = $element->getProperties()->getSizeEnum();

			return [
				"NAME" => $element->getName(),
				"COLOR" => $props ? $props->getColor() : null,
				"SIZE" => $sizeEnum ? $sizeEnum->getValue() : null,
			];
		}

		return null;
	}

	/**
	 * @param $fields
	 * @return AddResult
	 * @throws BitrixException
	 */
	public static function Add($fields)
	{
		$element = new ProductObject();

		$element->setName($fields["NAME"]);
		$element->setProperties((new ProductPropertiesObject())
			->setColor($fields["COLOR"])
			->setSize(ProductPropertiesObject::getSizeEnumId($fields["SIZE"])));

		return $element->save();
	}

	/**
	 * @param $id
	 * @return DeleteResult
	 * @throws \Exception
	 */
	public static function Delete($id)
	{
		return ProductTable::delete($id);
	}

	/**
	 * @param $id
	 * @param $fields
	 * @return UpdateResult|null
	 * @throws BitrixException
	 */
	public static function Update($id, $fields)
	{
		/** @var ProductObject $element */
		$element = ProductTable::query()
			->setSelect(['*', 'PROPERTIES'])
			->where('ID', $id)
			->exec()
			->fetchObject();

		if (!$element) {
			return null;
		}

		if (array_key_exists("NAME", $fields)) {
			$element->setName($fields["NAME"]);
		}

		$properties = $element->getProperties();
		if (!$properties) {
			$element->setProperties(new ProductPropertiesObject());
			$properties = $element->getProperties();
		}

		if (array_key_exists("COLOR", $fields)) {
			$properties->setColor($fields["COLOR"]);
		}

		if (array_key_exists("SIZE", $fields)) {
			$properties->setSize(ProductPropertiesObject::getSizeEnumId($fields["SIZE"]));
		}

		return $element->save();
	}

}