<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
	die();
}

use Bitrix\Main\Loader;


include_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/vendor/autoload.php");
