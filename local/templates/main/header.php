<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

global $APPLICATION;

?><!DOCTYPE html>
<html lang="<?= LANGUAGE_ID ?>">
<head>
	<meta charset="<?= SITE_CHARSET ?>">
	<meta http-equiv="Content-Type" content="text/html; charset=<?= LANG_CHARSET ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no, user-scalable=no">
	<?php $APPLICATION->ShowHead(false); ?>
	<title><? $APPLICATION->ShowTitle(); ?></title>
</head>
<body>
<div id="panel">
	<? $APPLICATION->ShowPanel(); ?>
</div>