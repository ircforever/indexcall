<?php

use Bitrix\Main\Loader;
use Logema\Test\ProductManager;

class LogemaTestComponent extends CBitrixComponent
{
	public function onPrepareComponentParams($arParams)
	{
		if (!Loader::includeModule("logema.test")) {
			ShowError("Не подключен модуль logema.test");
		}
	}

	public function executeComponent()
	{
		if ($this->request->isAjaxRequest()) {
			$this->handleAjax();
		}

		$this->includeComponentTemplate();
	}

	private function handleAjax()
	{
		if (!check_bitrix_sessid()) {
			return $this->sendAjaxResult([
				'error' => 'sessid'
			]);
		}

		try {
			$request = $this->request->getPostList()->toArray();
			$response = null;

			switch ($request["ACTION"]) {
				case "Add":
					$result = ProductManager::Add($request);
					if ($result->isSuccess()) {
						$response = ['success' => true, 'id' => $result->getId()];
					} else {
						$response = ['success' => false, 'errors' => $result->getErrorMessages()];
					}
					break;
				case "Delete":
					$result = ProductManager::Delete($request["ID"]);
					if ($result->isSuccess()) {
						$response = ['success' => true];
					} else {
						$response = ['success' => false, 'errors' => $result->getErrorMessages()];
					}
					break;
				case "Update":
					$result = ProductManager::Update($request["ID"], $request);
					if ($result->isSuccess()) {
						$response = ['success' => true];
					} else {
						$response = ['success' => false, 'errors' => $result->getErrorMessages()];
					}
					break;
				case "GetInfo":
					$info = ProductManager::GetInfo($request["ID"]);
					$response = $info ?: ['error' => 'not found'];
					break;
			}

			if ($response) {
				return $this->sendAjaxResult($response);
			}

			return $this->sendAjaxResult([
				'error' => 'empty result'
			]);

		} catch (\Exception $e) {
			return $this->sendAjaxResult([
				'exception' => $e->getMessage(),
				'trace' => $e->getTraceAsString()
			]);
		}
	}

	private function sendAjaxResult($result)
	{
		$GLOBALS['APPLICATION']->RestartBuffer();

		echo print_r($result, true);

		CMain::FinalActions();
		die();
	}
}