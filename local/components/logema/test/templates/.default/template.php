<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
/**
 * @var array $arParams
 * @var array $arResult
 * @global \CMain $APPLICATION
 * @global \CUser $USER
 * @global \CDatabase $DB
 * @var \CBitrixComponentTemplate $this
 * @var string $componentPath
 * @var string $templateName
 * @var string $templateFile
 * @var string $templateFolder
 * @var string $templateData
 */
$this->setFrameMode(true);
?>
<form action="">
	<?= bitrix_sessid_post() ?>
	<div>
		<p>ID</p>
		<input type="text" name="ID"/>
	</div>
	<div>
		<p>Название</p>
		<input type="text" name="NAME"/>
	</div>
	<div>
		<p>Цвет</p>
		<input type="text" name="COLOR"/>
	</div>
	<div>
		<p>Размер</p>
		<input type="text" name="SIZE"/>
	</div>
	<div>
		<p>Действие</p>
		<select name="ACTION">
			<option value="Add" selected>Add</option>
			<option value="Delete">Delete</option>
			<option value="Update">Update</option>
			<option value="GetInfo">GetInfo</option>
		</select>
	</div>
	<br>
	<div>
		<button type="submit">Отправить</button>
	</div>
</form>
<br>
<div id="result" style="white-space: pre"></div>
<script>
	$(function () {
		var result = $('#result');

		$('form').on('submit', function (e) {
			$.ajax('', {
				method: 'POST',
				data: $(this).serializeArray()
			}).then(function (data) {
				result.html(data);
			});
			return e.preventDefault();
		})
	});
</script>
